﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
    public TextMeshProUGUI textSpeed;
    public TextMeshProUGUI textDamage;
    private int numSpeed = 0, numDamage = 0;
    private string speedToText, damageToText;

    // Start is called before the first frame update
    void Start()
    {
        numSpeed = Random.Range(1, 15);
        numDamage = Random.Range(1, 15);
    }

    // Update is called once per frame
    void Update()
    {
        speedToText = numSpeed.ToString();
        damageToText = numDamage.ToString();
        textSpeed.text = speedToText;
        textDamage.text = damageToText;
        StartCoroutine(AfterMatch());
    }

    IEnumerator AfterMatch() {
        yield return new WaitForSeconds(15f);
        SceneManager.LoadScene("Gameplay");
    }
}
