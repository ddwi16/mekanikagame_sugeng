﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Progressbar : LoadingBar
{
    private UnityEvent onProgressComplete;

    public new float CurrentValue {
        get {
            return base.CurrentValue;
        }
        set {
            if(CurrentValue >= slider.maxValue) {
                onProgressComplete.Invoke();
            }

            base.CurrentValue = value % slider.maxValue;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(onProgressComplete == null) {
            onProgressComplete = new UnityEvent();
        }
        onProgressComplete.AddListener(OnProgressComplete);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateValue(0.003f);
        if(slider.value > 0.4) {
            UpdateValue(0.000001f);
        }
    }

    void UpdateValue(float setNum) {
        CurrentValue += setNum;
    }

    void OnProgressComplete() {
        Debug.Log("Progress Complete");
    }
}
