﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public PlayerStatistik myPlayer;
    public GameObject gameOver;

    [Header("Health")]
    public int maxHealth;
    public int currentHealth;
    public HealthBar healthBar;

    [Space]
    [Header("Mouse and Movement")]
    public Rigidbody2D rb;
    private Vector2 _mousePos;
    public Camera cam;

    void Start()
    {
        maxHealth = myPlayer.playerHealth;
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        _mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        PlayerDie();
    }

    private void FixedUpdate()
    {
        Vector2 lookDirection = _mousePos - rb.position;
        float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }

    void PlayerDie() {
        if(currentHealth <= 0) {
            gameOver.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Enemy") {
            Debug.Log("Kena");
            currentHealth -= 10;
            healthBar.SetHealth(currentHealth);
        }
        
    }
}
