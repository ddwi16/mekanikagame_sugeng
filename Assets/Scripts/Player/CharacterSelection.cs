﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelection : MonoBehaviour
{
    public GameObject countdownTimer;
    public Sprite[] player;
    public GameObject charSelect;
    public GameObject[] spawnEnemy;
    public bool yellowStatus = false;
    public bool blueStatus = false;
    public bool greenStatus = false;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RedPlay() {
        blueStatus = true;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = player[0];
        charSelect.SetActive(false);
        spawnEnemy[0].SetActive(true);
        countdownTimer.SetActive(true);
    }

    public void BluePlay() {
        greenStatus = true;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = player[1];
        charSelect.SetActive(false);
        spawnEnemy[1].SetActive(true);
        countdownTimer.SetActive(true);
    }

    public void GreenPlay() {
        yellowStatus = true;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = player[2];
        charSelect.SetActive(false);
        spawnEnemy[2].SetActive(true);
        countdownTimer.SetActive(true);
    }
}
