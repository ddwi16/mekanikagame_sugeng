﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defending : MonoBehaviour
{
    public Transform defendPos;
    public GameObject defendPrefab;
    public GameObject[] skillOrbs;
    private int useSkill = -1;

    // Update is called once per frame
    void Update()
    {
        if(useSkill <= 2) {
            if(Input.GetKeyDown(KeyCode.Space)) {
                PlayDefends();
                useSkill += 1;
                skillOrbs[useSkill].SetActive(false);
            }
        }
    }

    void PlayDefends() {
        Instantiate(defendPrefab, defendPos.position, defendPos.rotation);
    }
}
