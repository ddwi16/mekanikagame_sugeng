﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 180f;

    public TextMeshProUGUI countdownText;
    public GameObject[] objectDestroy;
    public GameObject winBar;

    // Start is called before the first frame update
    private void Awake()
    {
        currentTime = startingTime;    
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        if(currentTime <= 0) {
            currentTime = 0;
            winBar.SetActive(true);
            objectDestroy[objectDestroy.Length].SetActive(false);
        }
        countdownText.text = currentTime.ToString("0");
    }
}
