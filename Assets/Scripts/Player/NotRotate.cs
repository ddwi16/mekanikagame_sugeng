﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotRotate : MonoBehaviour
{
    Quaternion rotation;
    Animator animator;

    private void Awake() {
        rotation = transform.rotation;
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        animator.Play("RedPlayerAnim");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
