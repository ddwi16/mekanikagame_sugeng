﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Scenes Presets")]
    public GameObject MainMenu;
    public GameObject Credit;

    public void ToCredit() {
        MainMenu.SetActive(false);
        Credit.SetActive(true);
    }

    public void ToMainMenu() {
        Credit.SetActive(false);
        MainMenu.SetActive(true);
    }

    public void PlayGame() {
        SceneManager.LoadScene("Gameplay");
    }

    public void ExitOut() {
        Application.Quit();
    }
}
