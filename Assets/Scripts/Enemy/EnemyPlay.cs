﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;

[System.Serializable]
public class EnemyStats {

    private int enemyHealth = Random.Range(60, 100);
    private int enemyAttack = Random.Range(10, 40);
    private float enemySpeed = Random.Range(4, 12);

    public int getEnemyHealth() {
        return enemyHealth;
    }

    public float getEnemySpeed() {
        return enemySpeed;
    }

    public int getEnemyAttack() {
        return enemyAttack;
    }
}

public class EnemyPlay : MonoBehaviour
{
    [Header("Enemy Stats")]
    public EnemyStats myEnemyStats;
    public int health;
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    [Space]
    [Header("Enemy Property")]
    private float _timeToShoot;
    public float startTimeToShoot;
    public GameObject projectile;
    public Transform shootingPoint;
    
    private Transform _player;
    private Rigidbody2D _rigidbody;
            
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _rigidbody = GetComponent<Rigidbody2D>();
        speed = myEnemyStats.getEnemySpeed();
        health = myEnemyStats.getEnemyHealth();
    }

    // Update is called once per frame
    void Update()
    {
        LookAtPlayer();
        
        Debug.Log(myEnemyStats.getEnemyHealth());
        Debug.Log(myEnemyStats.getEnemySpeed());
        
        if (Vector2.Distance(transform.position, _player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.position, speed * Time.deltaTime);
        }
        else if(Vector2.Distance(transform.position, _player.position) < stoppingDistance  && Vector2.Distance(transform.position, _player.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        }
        else if(Vector2.Distance(transform.position, _player.position) < retreatDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.position, -speed * Time.deltaTime);
        }

        if (_timeToShoot <= 0)
        {
            Instantiate(projectile, shootingPoint.position, shootingPoint.rotation);
            _timeToShoot = startTimeToShoot;
        }
        else
        {
            _timeToShoot -= Time.deltaTime;
        }
    }

    void LookAtPlayer() {
        Vector3 direction = _player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        _rigidbody.rotation = angle;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "BulletPlayer") {
            Debug.Log("kenek enemy " + health);
            health -= 20;

            if(health <= 0) {
                Destroy(gameObject);
            }            
        }
    }
}
