﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieScript : MonoBehaviour
{
    private Transform _player;
    private Rigidbody2D _rigidbody;
    private float speed = 8f;
    private Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        LookAtPlayer();
    }
    
    void LookAtPlayer() {
        Vector3 direction = _player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        _rigidbody.rotation = angle;
        direction.Normalize();
        movement = direction;
    }

    private void FixedUpdate() {
        MoveCharacter(movement);
    }

    void MoveCharacter(Vector2 direction) {
        _rigidbody.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player") {
            Destroy(gameObject);
        }
    }
}
