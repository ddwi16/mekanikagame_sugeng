﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuStatsShow : MonoBehaviour
{
    public PlayerStatistik myPlayer;

    GameManager gameManager;

    [Space]
    [Header("HUD Player Name")]
    public GameObject inputFieldID;
    public string idPlayer;
    public TextMeshProUGUI id_Display;

    [Space]
    [Header("HUD Player Level & Rank")]
    public TextMeshProUGUI aboutRank, aboutLevel;
    public int rankStatus, levelStatus;

    void Start()
    {
        id_Display = GetComponent<TextMeshProUGUI>();
        aboutRank = GetComponent<TextMeshProUGUI>();
        aboutLevel = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        aboutRank.text = rankStatus.ToString();
        aboutLevel.text = levelStatus.ToString();
        id_Display.text = myPlayer.playerName;
    }

    public void StatPlayerShow() {
        rankStatus = Random.Range(5, 10);
        levelStatus = Random.Range(1, 10);
    }
}
