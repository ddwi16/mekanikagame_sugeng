﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveManager : MonoBehaviour
{
    private PlayerStats playerStats;

    private void Awake() {
        playerStats = GameObject.FindObjectOfType<PlayerStats>();
        Load();
    }

    public void Save() {
        Debug.Log("Saved!");
        FileStream file = new FileStream(Application.persistentDataPath + "/Player.uwu", FileMode.OpenOrCreate);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(file, playerStats.myPlayer);
        file.Close();
    }

    public void Load() {
        FileStream file = new FileStream(Application.persistentDataPath + "/Player.uwu", FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        //playerStats.myPlayer = (MyPlayerStats) formatter.Deserialize(file);
        file.Close();
    }
}
