﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    public GameObject pauseGame;

    public void PauseGame() {
        pauseGame.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame() {
        pauseGame.SetActive(false);
        Time.timeScale = 1f;
    }

    public void BackToHome() {
        SceneManager.LoadScene("MainMenu");
    }
}
