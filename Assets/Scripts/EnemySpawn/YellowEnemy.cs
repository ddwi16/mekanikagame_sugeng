﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YellowEnemy : MonoBehaviour
{
    public Transform[] spawnEnemy;
    public GameObject enemyPrefabs;

    [Space]
    [Header("Stats Gameplay")]
    public Text peopleInRoom;
    public int valuePeople = 50;

    void Start()
    {
        InvokeRepeating("GenerateEnemyPhaseOne", 5.0f, 4.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(valuePeople <= 1) {
            CancelInvoke("GenerateEnemyPhaseOne");
        }
        peopleInRoom.text = "50/" + valuePeople.ToString();
    }

    void GenerateEnemyPhaseOne() {
        Instantiate(enemyPrefabs,
            spawnEnemy[Random.Range(0,spawnEnemy.Length)].position, 
            spawnEnemy[Random.Range(0,spawnEnemy.Length)].rotation);
        
        valuePeople -= 1;
    }
}
