﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
    Animator _animation;
    IEnumerator WaitLoadScene() {
        yield return new WaitForSeconds(22);
        SceneManager.LoadScene("Gameplay");
    }
    void Update()
    {
        StartCoroutine(WaitLoadScene());
    }
}
